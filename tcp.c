#include "duktape/duktape.h"
#include "main.h"

#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <unistd.h> /* close() */

#define NSOCKETS_MAX 128

char tcp_buf[4096];
int tcp_sockets[NSOCKETS_MAX];
int tcp_nsockets = 0;

char *tcp_error = NULL; /* if tcp_connect() fails it returns -1 and this will be set */

int tcp_connect(const char *hostname, const char *servname) {
	int ret;
	struct addrinfo hints, *addrinfo;
	/* Get adress information. */
	memset((void *) &hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	ret = getaddrinfo(hostname, servname, &hints, &addrinfo);
	if(ret) {
		tcp_error = (char *) gai_strerror(ret);
		return -1;
	}
	/* Create socket. */
	if((ret = socket(addrinfo->ai_family, addrinfo->ai_socktype, addrinfo->ai_protocol)) == -1) {
		tcp_error = strerror(errno);
		freeaddrinfo(addrinfo);
		return -1;
	}
	/* Connect. */
	if(connect(ret, addrinfo->ai_addr, addrinfo->ai_addrlen) == -1) {
		tcp_error = strerror(errno);
		freeaddrinfo(addrinfo);
		close(ret);
		return -1;
	}
	/* Free address information. */
	freeaddrinfo(addrinfo);
	/* Return. */
	tcp_sockets[tcp_nsockets++] = ret;
	return ret;
}

void tcp_close(int sock) {
	int i;
	for (i = 0; i < tcp_nsockets; ++i)
		if (tcp_sockets[i] == sock)
			break;
	if (i == tcp_nsockets)
		return;
	tcp_sockets[i] = tcp_sockets[--tcp_nsockets];
	close(sock);
}

static duk_ret_t native_tcp_connect(duk_context *ctx) {
	const char *host, *serv;
	int sock;
	host = duk_require_string (ctx, 0);
	serv = duk_require_string(ctx, 1);
	sock = tcp_connect(host, serv);
	if (sock == -1) {
		duk_push_error_object(ctx, DUK_ERR_TYPE_ERROR, "can't connect: %s", tcp_error);
		return duk_throw(ctx);
	}
	duk_push_uint(ctx, sock);
	return 1;
}

static duk_ret_t native_tcp_close(duk_context *ctx) {
	int sock = duk_require_uint(ctx, 0);
	tcp_close(sock);
	return 0;
}

static duk_ret_t native_tcp_send(duk_context *ctx) {
	int sock = duk_require_uint(ctx, 0);
	duk_size_t len;
	const char *str = duk_require_lstring(ctx, 1, &len);
	send(sock, str, len, 0);
	return 0;
}

void tcp_onreceive(int sock) {
	ssize_t len = recv(sock, tcp_buf, sizeof(tcp_buf) - 1, MSG_DONTWAIT);
	if (len <= 0) { /* Disconnected or error. */
		tcp_close(sock);
		duk_get_global_string(ctx, "tcp_onclose");
		duk_push_uint(ctx, sock);
		duk_call(ctx, 1);
		duk_pop(ctx);
		return;
	}
	duk_get_global_string(ctx, "tcp_onreceive");
	duk_push_uint(ctx, sock);
	duk_push_lstring(ctx, tcp_buf, len);
	duk_call(ctx, 2);
	duk_pop(ctx);
}

void tcp_init() {
	duk_push_c_function(ctx, native_tcp_connect, 2);
	duk_put_global_string(ctx, "tcp_connect");
	duk_push_c_function(ctx, native_tcp_close, 1);
	duk_put_global_string(ctx, "tcp_close");
	duk_push_c_function(ctx, native_tcp_send, 2);
	duk_put_global_string(ctx, "tcp_send");
}

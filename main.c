#include "duktape/duktape.h"
#include "file.h"
#include "ui.h"
#include "tcp.h"

#include <stdlib.h> /* atexit(), getenv(), EXIT_FAILURE, EXIT_SUCCESS */
#include <stdio.h> /* fprintf(), stderr */
#include <signal.h> /* signal() */
#include <unistd.h> /* STDIN_FILENO */
#include <sys/select.h> /* select() */
#include <string.h> /* strerror() */
#include <errno.h> /* errno */

duk_context *ctx;

static void dfatal(void *udata, const char *msg) {
	// TODO somehow report line number
    ui_deinit();
    fprintf(stderr, "error (JS): %s\n", (msg != NULL) ? msg : "no message");
    fflush(stderr);
    exit(EXIT_FAILURE);
}

static duk_ret_t native_getenv(duk_context *ctx) {
	const char *vn = duk_require_string(ctx, 0);
	const char *s = getenv(vn);
	duk_push_string(ctx, s);
	return 1;
}

static duk_ret_t native_exit(duk_context *ctx) {
	int rc = duk_require_int(ctx, 0);
	exit(rc);
	return 0;
}

void sighandler(int signal) {
	exit(EXIT_FAILURE);
}

void finalize() {
	duk_destroy_heap(ctx);
}

int main(int argc, char *argv[]) {
	ctx = duk_create_heap(NULL, NULL, NULL, NULL, dfatal);
	if (ctx == NULL) {
		fprintf(stderr, "error: can't create duk context\n");
		return EXIT_FAILURE;
	}

	atexit(finalize);
	signal(SIGTERM, sighandler);
	signal(SIGINT, sighandler);
	signal(SIGHUP, sighandler);

	duk_push_c_function(ctx, native_getenv, 1);
	duk_put_global_string(ctx, "getenv");
	duk_push_c_function(ctx, native_exit, 1);
	duk_put_global_string(ctx, "exit");

	tcp_init();
	ui_init();

	char *script = file_read("main.js");
	if (script != NULL) {
		duk_eval_string(ctx, script);
	} else {
		fprintf(stderr, "error: can't read main script\n");
		return EXIT_FAILURE;		
	}

	/* Call oninit(). */
	duk_get_global_string(ctx, "oninit");
	duk_call(ctx, 0);
	duk_pop(ctx);

	while (1) {
		struct timeval tv = { .tv_sec = 10, .tv_usec = 0 };
		fd_set fds;
		int nfds = STDIN_FILENO + 1, i;
		FD_ZERO(&fds);
		FD_SET(STDIN_FILENO, &fds);
		for (i = 0; i < tcp_nsockets; ++i) {
			FD_SET(tcp_sockets[i], &fds);
			if (tcp_sockets[i] + 1 > nfds)
				nfds = tcp_sockets[i] + 1;
		}
		if (select(nfds, &fds, NULL, NULL, &tv) == -1) {
			if (errno == EINTR)
				continue;
			ui_deinit();
			fprintf(stderr, "error: %s\n", strerror(errno));
			fflush(stderr);
			return EXIT_FAILURE;
		}
		if (FD_ISSET(STDIN_FILENO, &fds))
			ui_onchar();
		for (i = 0; i < tcp_nsockets; ++i) {
			if (FD_ISSET(tcp_sockets[i], &fds))
				tcp_onreceive(tcp_sockets[i]);
		}
	}

	/* Should never be reached */
	return EXIT_FAILURE;
}

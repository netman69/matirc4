#include "duktape/duktape.h"
#include "main.h"
#include "ui.h"

#include <termios.h> /* tcgetattr(), tcsetattr() */
#include <unistd.h> /* write(), fsync(), tcgetattr(), tcsetattr(), STDOUT_FILENO */
#include <string.h> /* memcpy(), strlen() */
#include <stdlib.h> /* atexit() */
#include <sys/ioctl.h> /* ioctl() */
#include <signal.h> /* signal() */

struct termios tios, tios_orig;
struct winsize sz;
const char *ui_init_str = "\e[?1049h"; /* Turn terminal to "private mode". */
const char *ui_deinit_str = "\e[0m\e[2J\e[1;1H\e[r\e[?1049l"; /* Clear screen + restore normal mode. */
/* Reason we clear the screen is because maybe not all terminals support "private mode".
 * Earlier we had:
 *   const char *ui_deinit_str = "\e[0m\e[2J\e[1;1H\ec";
 * But \ec (reset terminal state) interferes with restoring from private mode.
 * "\e[r" is needed then to restore the scrolling window stuff (DECSTBM).
 */

int ui_initialized = 0;

static duk_ret_t native_write(duk_context *ctx) {
	const char *str;
	duk_push_string(ctx, "");
	duk_insert(ctx, 0);
	duk_join(ctx, duk_get_top(ctx) - 1);
	str = duk_safe_to_string(ctx, -1);
	write(STDOUT_FILENO, str, strlen(str));
	return 0;
}

static duk_ret_t native_sync(duk_context *ctx) {
	fsync(STDOUT_FILENO);
	return 0;
}

void ui_setsize() {
	ioctl(0, TIOCGWINSZ, &sz);
	duk_push_int(ctx, sz.ws_col);
	duk_put_global_string(ctx, "ui_cols");
	duk_push_int(ctx, sz.ws_row);
	duk_put_global_string(ctx, "ui_rows");
}

void ui_resize(int signal) {
	ui_setsize();
	/* Call onresize(). */
	duk_get_global_string(ctx, "ui_onresize");
	duk_call(ctx, 0);
	duk_pop(ctx);
}

void ui_deinit();

void ui_onchar() {
	unsigned char seq[4];
	duk_get_global_string(ctx, "ui_onchar");
	read(STDIN_FILENO, seq, 1);
	switch (seq[0] >> 3) {
		case 0x1E:
			read(STDIN_FILENO, seq + 1, 3);
			duk_push_lstring(ctx, (const char *) seq, 4);
			break;
		case 0x1C:
		case 0x1D:
			read(STDIN_FILENO, seq + 1, 2);
			duk_push_lstring(ctx, (const char *) seq, 3);
			break;
		case 0x18:
		case 0x19:
		case 0x20:
		case 0x21:
			read(STDIN_FILENO, seq + 1, 1);
			duk_push_lstring(ctx, (const char *) seq, 2);
			break;
		default:
			duk_push_lstring(ctx, (const char *) seq, 1);
	}
	duk_call(ctx, 1);
	duk_pop(ctx);
}

void ui_deinit() {
	if (ui_initialized == 0)
		return;
	tcsetattr(STDOUT_FILENO, TCSANOW, &tios_orig);
	write(STDOUT_FILENO, ui_deinit_str, strlen(ui_deinit_str));
	fsync(STDOUT_FILENO);
	ui_initialized = 0;
}

void ui_resume() {
	tcsetattr(STDOUT_FILENO, TCSANOW, &tios);
	ui_resize(0);
}

void ui_init() {
	write(STDOUT_FILENO, ui_init_str, strlen(ui_init_str));
	fsync(STDOUT_FILENO);
	/* Do termios magic to turn off echoing etc. */
	tcgetattr(STDOUT_FILENO, &tios);
	memcpy(&tios_orig, &tios, sizeof(tios_orig));
	cfmakeraw(&tios);
	tios.c_iflag &= ~IGNBRK;
	tios.c_iflag |= BRKINT;
	tios.c_lflag |= ISIG;
	tcsetattr(STDOUT_FILENO, TCSANOW, &tios);
	atexit(ui_deinit);
	signal(SIGWINCH, ui_resize);
	signal(SIGCONT, ui_resume);
	ui_initialized = 1;
	/* Duktape stuff. */
	ui_setsize();
	duk_push_c_function(ctx, native_write, DUK_VARARGS);
	duk_put_global_string(ctx, "ui_write");
	duk_push_c_function(ctx, native_sync, 0);
	duk_put_global_string(ctx, "ui_sync");
}

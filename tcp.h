#ifndef __TCP_H__
#define __TCP_H__

extern int tcp_sockets[];
extern int tcp_nsockets;

void tcp_onreceive(int sock);
extern void tcp_init();

#endif /* __TCP_H__ */

CFLAGS  += -Wall
LDFLAGS += -lm
OBJS    = main.o ui.o tcp.o file.o duktape/duktape.o

all: matirc

matirc: $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $(OBJS)

clean:
	rm -rf $(OBJS) matirc

#include <sys/types.h> /* open(), fstat() */
#include <sys/stat.h> /* open(), fstat() */
#include <fcntl.h> /* open() */
#include <unistd.h> /* read(), fstat() */
#include <stdlib.h> /* malloc(), NULL */

char *file_read(char *path) { /* Read a text file to memory, as 0-terminated string. */
	struct stat sb;
	char *buf = NULL;
	int r = 0, fd = open(path, O_RDONLY);
	if(fd > 0) {
		if(fstat(fd, &sb) == 0) {
			buf = (char *) malloc(sb.st_size + 1);
			if(buf == NULL)
				return NULL;
			r = read(fd, (void *) buf, sb.st_size);
			if(r <= 0) {
				free((void *) buf);
				return NULL;
			} else buf[r] = 0;
		}
		close(fd);
	}
	return buf;
}

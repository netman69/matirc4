/*
 * Functions the C code expects to be implemented:
 *   oninit() - run on start
 *
 *   ui_onresize() - called if terminal resized
 *   ui_onchar(c) - called when a character comes in from stdin, argument is a string
 *
 *   tcp_onreceive(socket, data) - called when tcp data comes in
 *   tcp_onclose(socket) - called when remote host disconnects
 *
 * Available functions and variables are:
 *   ui_rows - number of rows
 *   ui_cols - number of columns
 *   ui_write(str, ...) - write text
 *   ui_sync() - flush buffered text
 *   exit(s) - exit with return status s
 *   getenv(v) - get environment variable, returns null on failure
 *
 *   tcp_connect(hostname, servname) - connect to tcp server, returns socket
 *   tcp_send(socket, data) - send data to tcp socket
 *   tcp_close(socket)
 *
 */

// TODO show the time somewhere

var color_bg = ui_color(0, 37, 40);
var color_topbar = ui_color(0, 37, 44);
var color_botbar = ui_color(0, 37, 44);
var color_botbar_tab = ui_color(0, 37, 44);
var color_botbar_tab_hl = ui_color(0, 31, 44);
var color_botbar_tab_active = ui_color(1, 37, 44);
var color_botbar_scrollarrow = ui_color(0, 31, 44)
var color_input = color_bg + ui_color(0, 37, 40);
var color_input_ins = color_bg + ui_color(0, 93, 40);
var color_input_nick = ui_color(0, 32, 40);
var color_input_scrollarrows = ui_color(0, 31, 40);
var color_message_own = ui_color(0, 32, 40);
var color_message_remote = ui_color(0, 37, 40);
var color_message_system = ui_color(0, 31, 40);

var scrollback_max = 4096;
var inputhistory_max = 512;

var input;

function oninit() {
	input = new Input();

	input.onenter = function(str) {
		if (str.startsWith("/"))
			handlecommand(str);
		else handleinput(str);
	}

	// TODO replace this with actually useful shit
	input.completion.add(function() {
			var left = input.substring(0, input.pos);
			return left.startsWith("/") && !(/ /.test(left));
		}, true, false, true, function() { return ["server", "nick", "ping"]; });
	input.completion.add(function() { return true; }, false, true, true, function() {
		return tabs.current.names || [];
	});

	ui_oninit();
}

function handlecommand(str) {
	var cmd = str.split(" ");
	for (var i = 0; i < cmd.length; ++i) {
		if (cmd[i] == "") { /* Remove emtpy elements */
			cmd.splice(i, 1);
			--i;
		}
	}
	switch (cmd[0].toLowerCase()) {
		case "/server":
			// TODO way to choose protocol
			if (cmd.length < 2 || cmd.length > 3) {
				tabs.current.writeline(color_message_system + "error: ", "invalid number of arguments to server command");
				break;
			}
			if (tabs.current.onclose)
				tabs.current.onclose();
			try {
				var tab = new Tab();
				tab.nick = tabs.current.nick;
				tab.scrollback = tabs.current.scrollback;
				tabs.current = new ProtoIRC(cmd[1], cmd[2], tab);
			} catch(e) {
				tabs.current.writeline(color_message_system + "error: ", e.toString());
			}
			ui_drawtopbar();
			ui_drawbotbar();
			break;
		case "/nick":
			if (cmd.length != 2) {
				tabs.current.writeline(color_message_system + "error: ", "invalid number of arguments to nick command");
				break;
			}
			tabs.current.nick = cmd[1];
			ui_drawinput();
			break;
		default:
			tabs.current.writeline(color_message_system + "unknown command: ", cmd[0]);
	}
}

function handleinput(str) {
	if (tabs.current.handleinput)
		tabs.current.handleinput(str);
}

/***********************************/
/* Protocol implemenation for IRC. */
/***********************************/

function ProtoIRC(host, serv, tab) {
	serv = (serv || "6667");
	Object.assign(this, tab);
	this.buf = "";
	this.sock = tcp_connect(host, serv);
	this.name = this.server = host + ":" + serv;
	this.tabs = [];
	tcp_send(this.sock, "NICK " + this.nick + "\r\n"); // TODO do this again when nick command runs on open server, DO NOT FORGET to change nick in subtabs too then
	tcp_send(this.sock, "USER " + this.nick + " 0 * :" + this.nick + "\r\n"); // TODO check if this is the right thing to do
}

ProtoIRC.prototype = new Tab;

ProtoIRC.prototype.tcp_onreceive = function(data) {
	this.buf += data.replace(/\x1B/g, "^[").replace(/\t/g, " "); /* Remove escapes and tabs from received data. */
	var tab = this;
	this.buf = this.buf.replace(/([^\r\n]*)\r?\n/g, function(m, ln) {
		tab.handleline(ln);
		return "";
	});
};

ProtoIRC.prototype.tcp_onclose = function() {
	this.writeline("", color_message_system + "disconnected");
	this.sock = undefined;
};

ProtoIRC.prototype.onclose = function() {
	if (this.sock)
		tcp_close(this.sock);
	this.sock = undefined;
};

ProtoIRC.prototype.handleinput = function(str) {
	if (!this.sock) // TODO error that disconnected, maybe simply replace this function by Tab.handlinenput
		return;
	this.writeline("", color_message_own + str); // TODO
	tcp_send(this.sock, str + "\r\n");
};

ProtoIRC.prototype.handleline = function(ln) { /* See RFC1459, section 2.3.1. */
	var res = /^(\:[^ ]* +|)([a-zA-Z]+|[0-9]{3})( +.*|)$/.exec(ln);
	if (res) {
		var prefix = function(str) {
			var user, host;
			str = str.replace(/^\:([^ ]*) +$/, "$1");
			str = str.replace(/\@(.*)$/, function(m, h) { host = h; return ""; });
			str = str.replace(/\!(.*)$/, function(m, u) { user = u; return ""; });
			return { name: str, user: user, host: host };
		}(res[1]);
		var command = res[2];
		var params = function(str) {
			var res, ret = [];
			var tra, rex = / +([^\: ][^ ]*)/g;
			str = str.replace(/ +\:(.*)$/, function(m, t) {
				tra = t;
				return "";
			});
			while (res = rex.exec(str))
				ret.push(res[1]);
			if (tra)
				ret.push(tra);
			return ret;
		}(res[3]);
		/*this.writeline(color_message_remote + "prefix: " + prefix.name);
		if (prefix.user)
			this.writeline(color_message_remote + " user: " + prefix.user);
		if (prefix.host)
			this.writeline(color_message_remote + " host: " + prefix.host);
		this.writeline(color_message_remote + "command: " + command);
		this.writeline(color_message_remote + "params: " + params.toString());*/
		this.handleirc(ln, prefix, command, params);
	} else this.writeline(color_message_system + "failed to parse:", " " + ln);
};

ProtoIRC.prototype.handleirc = function(ln, prefix, command, params) {
	switch (command) {
		case "NICK": /* This comes back for ourselves, but only if we're in a channel. */
			// Time-Warp change to gline
			//  unknown command: :Time-Warp!f0xy@mirbsd/fan/time-warp NICK :gline
			// gline change back to Time-Warp
			//  unknown command: :gline!f0xy@mirbsd/fan/time-warp NICK :Time-Warp

			break;
		case "PING":
			this.writeline(color_message_remote + "[ping-pong] ", params[0]);
			tcp_send(this.sock, "PONG :" + params[0] + "\r\n");
			break;
		case "JOIN": // TODO this also happens if other folks join the channel
			var tab = this.getsubtab(params[0], true);
			tab.writeline(color_bg, "Joined channel " + params[0]);
			break;
		case "PRIVMSG":
			if (params[0].startsWith("#")) {
				var tab = this.getsubtab(params[0], true);
				tab.writeline(color_message_remote + prefix.name + ": ", color_bg + params[1]);
			} else {
				var tab = this.getsubtab(prefix.name, true);
				tab.writeline(color_message_remote + prefix.name + ": ", color_bg + params[1]);
			}
			break;
		case "NOTICE":
			this.writeline(color_message_remote + "[notice] ", params[1]);
			break;
		case "331": /* RPL_NOTOPIC */
			//this.writeline(color_message_remote + "[topic] ", "No topic set for channel.");
			break;
		case "332": /* RPL_TOPIC */
			var tab = this.getsubtab(params[1], false) || this;
			tab.writeline(color_message_remote + "[topic] ", "Topic for " + params[1] + ": " + params[2]);
			break;
		case "353": /* NAMES list. */
			var tab = this.getsubtab(params[2], false);
			if (tab != undefined) {
				tab.names_new = ((tab.names_new) ? tab.names.concat(params[3].split(" ")) : params[3].split(" "));
				tab.writeline(color_message_remote + "[names] ", color_bg + params[3]);
			} else tabs.current.writeline(color_message_remote + "[names for " + params[2] + "] ", color_bg + params[3]);
			break;
		case "366":
			var tab = this.getsubtab(params[1], false);
			if (tab != undefined) {
				tab.names = tab.names_new;
				tab.names_new = undefined;
				tab.writeline(color_message_remote + "end of names", "");
			} else tabs.current.writeline(color_message_remote + "end of names for " + params[1], "");
			break;
		case "001":
			this.writeline(color_message_remote + "[welcome] ", params[1]);
			break;
		case "002":
			this.writeline(color_message_remote + "[host] ", params[1]);
			break;
		case "003":
			this.writeline(color_message_remote + "[created] ", params[1]);
			break;
		case "004":
			this.writeline(color_message_remote + "[server info]", "");
			this.writeline(color_message_remote + " servername:    ", params[1]);
			this.writeline(color_message_remote + " version:       ", params[2]);
			this.writeline(color_message_remote + " user modes:    ", params[3]);
			this.writeline(color_message_remote + " channel modes: ", params[4]);
			// TODO there's another param on freenode that wasn't described in RFC2812
			break;
		case "372":
		case "376":
			this.writeline(color_message_remote + "[MOTD] ", params[1]);
			break;
		default:
			this.writeline(color_message_system + "unknown command: ", ln);
	}
};

ProtoIRC.prototype.getsubtab = function(ch, create) {
	for (var i = 0; i < this.tabs.length; ++i)
		if (this.tabs[i].channel == ch)
			return this.tabs[i];
	return ((create) ? new ProtoIRCSubTab(this, ch) : undefined);
};

function ProtoIRCSubTab(stab, ch) {
	Tab.apply(this);
	this.ch = ch;
	this.name = ch;
	this.channel = ch;
	this.server = stab.name;
	this.nick = stab.nick;
	this.stab = stab;
	this.names = [];
	stab.tabs.push(this); // TODO remove from this list at onclose
	tabs.push(this); // TODO use right spot
	ui_drawbotbar();
}

ProtoIRCSubTab.prototype = Tab.prototype;

ProtoIRCSubTab.prototype.handleinput = function(str) {
	if (!this.stab.sock) // TODO error that disconnected, maybe simply replace this function by Tab.handlinenput
		return;
	this.writeline(color_message_own + this.nick + ": ", color_bg + str);
	tcp_send(this.stab.sock, "PRIVMSG " + this.ch + " :" + str + "\r\n");
};

/*****************************************************/
/* Protocol implemenation for taling over plain TCP. */
/*****************************************************/

function ProtoTCP(host, serv, tab) {
	serv = (serv || "1234");
	Object.assign(this, tab);
	this.buf = "";
	this.sock = tcp_connect(host, serv);
	this.name = this.server = host + ":" + serv;
}

ProtoTCP.prototype = new Tab;

ProtoTCP.prototype.tcp_onreceive = function(data) {
	this.buf += data.replace(/\x1B/g, "^[").replace(/\t/g, " "); /* Remove escapes and tabs from received data. */
	var tab = this;
	this.buf = this.buf.replace(/([^\r\n]*)\r?\n/g, function(m, ln) {
		tab.writeline(color_message_remote, ln);
		return "";
	});
};

ProtoTCP.prototype.tcp_onclose = function() {
	this.writeline(color_message_system, "disconnected");
};

ProtoTCP.prototype.onclose = function() {
	if (this.sock)
		tcp_close(this.sock);
	this.sock = undefined;
};

ProtoTCP.prototype.handleinput = function(str) {
	if (!this.sock)
		return;
	this.writeline(color_message_own, str);
	tcp_send(this.sock, str + "\n");
};

/******************/
/* VT100 helpers. */
/******************/

var ui_cls   = "\x1B[2J"; /* Clear screen. */
var ui_cel   = "\x1B[2K"; /* Clear entire line. */
var ui_sc    = "\x1B7"; /* Save cursor. */
var ui_rc    = "\x1B8"; /* Restore cursor. */
var ui_up    = "\x1BM"; /* Scroll up. */

function ui_color() {
	var c = "";
	for (var i = 0; i < arguments.length; ++i)
		c += arguments[i] + ";";
	return "\x1B[" + c.substring(0, c.length - 1) + "m";
}

function ui_cursor(y, x) {
	return "\x1B[" + (y + 1) + ";" + (x + 1) + "H";
}

function ui_scrollregion(top, bottom) {
	return "\x1B[" + (top + 1) + ";" + (bottom + 1) + "r";
}

function ui_space(n) {
	var ret = "";
	for (var i = 0; i < n; ++i)
		ret += " ";
	return ret;
}

/* Get length of visible characters in cols. */
Object.defineProperty(String.prototype, 'ui_length', {
	get: function() { return this.replace(/\x1B[0-9;\[]*[^0-9;\[]/g, "").length; }
});

Object.defineProperty(String.prototype, 'ui_colors', {
	get: function() {
		var rex = /\x1B\[[0-9;]*m/g, ret = "", v;
		while (v = rex.exec(this))
			ret += v;
		return ret;
	}
});

/* Substring that ignores escapes in the counts */
String.prototype.ui_substring = function(start, end) {
	end = (end || this.ui_length);
	var rstart;
	for (rstart = 0; start > 0; ++rstart, --start) {
		var m;
		while (m = /^\x1B[0-9;\[]*[^0-9;\[]/.exec(this.substring(rstart)))
			rstart += m[0].length;
	}
	var rend;
	for (rend = 0; end > 0; ++rend, --end) {
		var m;
		while (m = /^\x1B[0-9;\[]*[^0-9;\[]/.exec(this.substring(rend)))
			rend += m[0].length;
	}
	return this.substring(rstart, rend);
};

String.prototype.ui_substr = function(start, len) {
	return this.ui_substring(start, start + len);
};

/****************************/
/* Interface drawing stuff. */
/****************************/

function ui_oninit() {
	ui_draw();
}

function ui_onresize() {
	ui_draw();
}

function ui_draw() {
	ui_write(ui_scrollregion(1, ui_rows - 3));
	ui_drawtab();
	ui_drawtopbar();
	ui_drawbotbar();
	ui_drawinput();
	ui_sync();
}

function ui_drawtab() {
	tabs.current.draw();
}

function ui_drawtopbar() {
	var left = (tabs.current.channel || "");
	var right = "" + (tabs.current.server || tabs.current.name);
	var rmax = ui_cols - left.length - 3;
	var lmax = ui_cols - 2;
	rmax = ((rmax < 0) ? 0 : rmax);
	lmax = ((lmax < 0) ? 0 : lmax);
	right = right.substring(0, rmax);
	left = left.substring(0, lmax);
	ui_write(ui_sc + ui_cursor(0, 0) + color_topbar);
	ui_write(" " + left + ui_space(ui_cols - right.length - left.length - 2) + right + " " + ui_rc);
}

var ui_drawbotbar_scroll = 0;

function ui_drawbotbar() {
	var str = "";
	var cur_pos, cur_len;
	for (var i = 0; i < tabs.length; ++i) {
		if (i == tabs.current_index) {
			cur = "[ " + color_botbar_tab_active + tabs[i].name + color_botbar + " ] ";
			cur_pos = str.ui_length;
			cur_len = cur.ui_length;
			str += cur;
			tabs[i].act = false;
		} else if (tabs[i].act) {
			str += "[ " + color_botbar_tab_hl + tabs[i].name + color_botbar + " ] ";
		} else {
			str += "[ " + color_botbar_tab + tabs[i].name + color_botbar + " ] ";
		}
	}
	while (cur_pos + cur_len - ui_drawbotbar_scroll > ui_cols - 1)
		++ui_drawbotbar_scroll;
	while (cur_pos - ui_drawbotbar_scroll < 0)
		--ui_drawbotbar_scroll;
	while (str.ui_length - ui_drawbotbar_scroll < ui_cols - 1 && ui_drawbotbar_scroll > 0)
		--ui_drawbotbar_scroll; /* Make sure no needless scrolling, makes our life easier. */
	var sleft = (ui_drawbotbar_scroll > 0);
	var sright = (str.ui_length - ui_drawbotbar_scroll > ui_cols - 1);
	ui_write(ui_sc + ui_cursor(ui_rows - 2, 0) +
	         (sleft ? (color_botbar_scrollarrow + "<" + color_botbar) : (color_botbar + " ")) +
	         str.ui_substr(ui_drawbotbar_scroll, ui_cols - 2) + ui_space(ui_cols - str.ui_length - 2) +
			 (sright ? (color_botbar_scrollarrow + ">") : (color_botbar + " ")) + ui_rc);
}

function ui_drawinput() {
	var nick = "" + tabs.current.nick;
	ui_write(ui_cursor(ui_rows - 1, 0));
	ui_write(color_input + "[ " + color_input_nick + nick + color_input + " ]")
	input.y = ui_rows - 1;
	input.x = 5 + nick.length;
	input.width = ui_cols - input.x;
	input.draw();
}

function ui_onchar(c) {
	if (c == "\x1B") {
		input.isesc = true;
		input.esc = "\x1B";
	} else if (input.isesc) {
		input.esc += c;
		if (/[^0-9O;\[]/.test(c)) { /* O is for F1, F2, etc which come as "\x1BOP" etc. */
			input.isesc = false;
			ui_onkey(input.esc);
		}
	} else ui_onkey(c);
}

function ui_onkey(key) {
	if (input.onkey(key))
		return;
	switch (key) {
		/* Escape sequences. */
		case "\x1B[5~": /* PageUp. */
			for (var i = 0; i < ((ui_rows > 10) ? 5 : 1); ++i)
				tabs.current.scroll_up();
			break;
		case "\x1B[6~": /* PageDn. */
			for (var i = 0; i < ((ui_rows > 10) ? 5 : 1); ++i)
				tabs.current.scroll_dn();
			break;
		case "\x1B[1;5C": /* Ctrl-Right (only in xterm). */
			tabs.next();
			break;
		case "\x1B[1;5D": /* Ctrl-Left (only in xterm). */
			tabs.prev();
			break;
		/* Normal keys. */
		case "\x03": /* Ctrl-C. */
			exit(0);
			break;
		case "\x0C": /* Ctrl-L. */
			ui_draw();
			break;
		case "\x14": /* Ctrl-T. */
			tabs.add();
			break;
		case "\x17": /* Ctrl-W. */
			tabs.close();
			break;
		case "\x0E": /* Ctrl-N. */
			tabs.next();
			break;
		case "\x10": /* Ctrl-P. */
			tabs.prev();
			break;
	}
}

function ui_writeline(str) {
	if (ui_rows > 4) /* Need 2 rows for \r\n to work. */
		ui_write(ui_sc + ui_cursor(ui_rows - 3, 0) + color_bg + "\r\n" + str + ui_rc);
	else ui_write(ui_sc + ui_cursor(ui_rows - 3, 0) + color_bg + ui_cel + str + ui_rc);
}

/***********************/
/* Tabs related stuff. */
/***********************/

var tabs = [ new Tab() ];
tabs.current_index = 0;
Object.defineProperty(tabs, 'current', {
	get: function() { return this[this.current_index]; },
	set: function(t) { this[this.current_index] = t; }
});

tabs.next = function() {
	++this.current_index;
	if (this.current_index == this.length)
		this.current_index = 0;
	ui_draw();
};

tabs.prev = function() {
	--this.current_index;
	if (this.current_index == -1)
		this.current_index = ((this.length > 0) ? this.length - 1 : 0);
	ui_draw();
};

tabs.add = function(tab) {
	this.push(tab || new Tab());
	this.current_index = this.length - 1;
	ui_draw();
}

tabs.close = function() {
	if (this.current.onclose)
		this.current.onclose();
	if (this.length == 1)
		exit(0);
	this.splice(this.current_index, 1);
	if (this.current_index == this.length)
		--this.current_index;
	ui_draw();
}

function Tab() {
	this.name = "untitled";
	this.server = undefined;
	this.channel = undefined;
	this.nick = (getenv("USER") || "username");
	this.scrollback = [];
	this.scroll = 0;
	this.scroll_adj = 0;
	this.act = false;
}

Tab.prototype.writeline = function(prefix, str) {
	var line = new TabLine(prefix, str);
	var lines = line.wrap();
	if (tabs.current == this && !this.scroll) {
		for (var i = 0; i < lines.length; ++i)
			ui_writeline(lines[i]);
	} else {
		this.act = true;
		ui_drawbotbar();
	}
	if (this.scrollback.length > scrollback_max)
		this.scrollback.shift();
	this.scrollback.push(line);
};

Tab.prototype.scroll_up = function() {
	if (this.scrollback.length == 0)
		return;
	if (!this.scroll) {
		this.scroll = this.scrollback.length;
		this.scroll_adj = 0;
	}
	if (this.scrollback[this.scroll - 1].wrap().length - 1 > this.scroll_adj) {
		++this.scroll_adj;
	} else if (this.scroll > 1) {
		--this.scroll;
		this.scroll_adj = 0;
	} else return;
	var lines = this.getlines();
	var line = ((lines.length == ui_rows - 3) ? lines[0] : "");
	if (ui_rows > 4)
		ui_write(ui_sc + ui_cursor(1, 0) + color_bg + ui_up + line + ui_rc);
	else ui_write(ui_sc + ui_cursor(2, 0) + color_bg + ui_up + line + ui_rc);
};

Tab.prototype.scroll_dn = function() {
	if (!this.scroll || this.scrollback.length == 0)
		return;
	if (this.scroll_adj > 0 && this.scrollback[this.scroll - 1].wrap().length > 1) {
		while (this.scroll_adj >= this.scrollback[this.scroll - 1].wrap().length)
			--this.scroll_adj;
		--this.scroll_adj;
	} else {
		++this.scroll;
		this.scroll_adj = this.scrollback[this.scroll - 1].wrap().length - 1;
	}
	if (this.scroll >= this.scrollback.length && (this.scroll_adj == 0 || this.scrollback[this.scroll - 1].wrap().length == 1)) {
		this.scroll = 0;
		this.scroll_adj = 0;
	}
	var lines = this.getlines();
	ui_writeline(lines[lines.length - 1]);
};

Tab.prototype.draw = function() {
	var top = 1;
	var height = ui_rows - 3;
	var lines = this.getlines();
	if (this.scroll >= this.scrollback.length && (this.scroll_adj == 0 || this.scrollback[this.scroll - 1].wrap().length == 1))
		this.scroll = 0; /* This is to avoid resizing showing the full last line without scroll = 0 */
	ui_write(ui_sc + color_bg);
	for (var i = 0; i < height - lines.length; ++i) /* Clear the screen. */
		ui_write(ui_cursor(top + i, 0) + ui_cel);
	for (var i = 0; i < lines.length; ++i)
		ui_write(ui_cursor(top + height - lines.length + i, 0) + lines[i] + ui_space(ui_cols - lines[i].ui_length));
	ui_write(ui_rc); // TODO indicate somewhere whether we have scrolled or not
};

Tab.prototype.getlines = function() { /* Get the normally visible text as array of lines. */
	var top = 1;
	var height = ui_rows - 3;
	var scroll = (this.scroll || this.scrollback.length);
	var scrolltop = ((scroll - height > 0) ? scroll - height : 0);
	var rlines = this.scrollback.slice(scrolltop, scroll);
	var lines = [];
	for (var i = 0; i < rlines.length; ++i)
		lines = lines.concat(rlines[i].wrap());
	if (this.scroll && rlines.length > 0) { /* We need 1 line in rlines to do this. */
		var scroll_adj = this.scroll_adj;
		while (scroll_adj >= rlines[rlines.length - 1].wrap().length)
			--scroll_adj;
		lines = lines.slice(0, lines.length - scroll_adj);
	}
	return lines.slice(-height);
};

function TabLine(prefix, str) {
	this.prefix = prefix; /* Prefix part is excluded from word-wrap. */
	this.str = str;
}

TabLine.prototype.wrap = function() {
	var lines = [];
	var prefix = this.prefix;
	var cacc = ""; /* Color accumulator. */
	/* Break the prefix already until it'd fit, so we don't word-wrap it. */
	while (prefix.ui_length >= ui_cols) {
		var line = prefix.ui_substring(0, ui_cols);
		cacc += line.ui_colors;
		lines.push(cacc + line);
		prefix = prefix.ui_substring(ui_cols);
	}
	var words = this.str.split(" ");
	words[0] = prefix + words[0];
	while (words.length > 0) {
		var line = cacc;
		while (words.length > 0 && (words[0].ui_length + line.ui_length < ui_cols || line.ui_length == 0))
			line += ((line == cacc) ? "" : " ") + words.shift();
		if (line.ui_length > ui_cols) {
			words.unshift(line.ui_substring(ui_cols));
			line = line.ui_substring(0, ui_cols);
		}
		cacc += line.ui_colors;
		lines.push(line);
	}
	if (lines.length == 0)
		lines.push("");
	return lines;
};

function tcp_onreceive(sock, data) {
	for (var i = 0; i < tabs.length; ++i)
		if (tabs[i].sock == sock && tabs[i].tcp_onreceive)
			tabs[i].tcp_onreceive(data);
}

function tcp_onclose(sock) {
	for (var i = 0; i < tabs.length; ++i)
		if (tabs[i].sock == sock && tabs[i].tcp_onclose)
			tabs[i].tcp_onclose(sock);
}

/*********************/
/* Input processing. */
/*********************/

function Input(s) {
	this.value = s || "";
	this.pos = 0;
	this._insert = false;
	this.scroll = 0;
	this.isesc = false;
	this.esc = "";
	this.history = new InputHistory(this);
	this.completion = new InputCompletions(this);
	this.y = 0;
	this.x = 0; /* It also takes up 1 col to the left, for the scroll arrow. */
	this.width = 0;

	Object.defineProperty(this, 'length', {
		get: function() { return this.value.length; }
	});

	Object.defineProperty(this, 'insert', {
		get: function() { return this._insert; },
		set: function(v) {
			this._insert = v;
			this.draw();
		}
	});
};

Input.prototype = new String;

Input.prototype.valueOf = Input.prototype.toString = function() {
	return this.value;
};

Input.prototype.left = function() {
	if (this.pos == 0)
		return;
	this.completion.end();
	--this.pos;
	this.draw();
};

Input.prototype.right = function() {
	if (this.pos == this.length)
		return;
	this.completion.end();
	++this.pos;
	this.draw();
};

Input.prototype.home = function() {
	if (this.pos == 0)
		return;
	this.completion.end();
	this.pos = 0;
	this.draw();
};

Input.prototype.end = function() {
	if (this.pos == this.length)
		return; /* So completion only ends if something actually happens. */
	this.completion.end();
	this.pos = this.length;
	this.draw();
};

Input.prototype.del = function() {
	if (this.pos == this.length)
		return;
	this.completion.end();
	this.value = this.value.substring(0, this.pos) + this.value.substring(this.pos + 1, this.length);
	this.draw();
};

Input.prototype.backspace = function() {
	if (this.pos == 0)
		return;
	this.completion.end();
	this.value = this.value.substring(0, this.pos - 1) + this.value.substring(this.pos, this.length);
	--this.pos;
	this.draw();
};

Input.prototype.write = function(str) {
	this.completion.end();
	if (this.insert && this.pos < this.length)
		this.value = this.substring(0, this.pos) + str + this.substring(this.pos + 1, this.length);
	else this.value = this.substring(0, this.pos) + str + this.substring(this.pos, this.length);
	++this.pos;
	this.draw();
}

Input.prototype.clear = function() {
	this.value = "";
	this.pos = 0;
	this.draw();
}

Input.prototype.enter = function() {
	if (this == "")
		return;
	if (this.onenter)
		this.onenter(this.value);
	this.history.step();
	this.clear();
}

Input.prototype.draw = function() {
	while (this.pos - this.scroll > this.width - 1)
		++this.scroll;
	if (this.pos - this.scroll == this.width - 1 && this.length - this.scroll > this.width)
		++this.scroll; /* Scroll tiny bit more if the cursor would overlap the arrow */
	while (this.pos < this.scroll)
		--this.scroll;
	while (this.length - this.scroll < this.width - 1 && this.scroll > 0)
		--this.scroll; /* Avoid needless scrolling. */
	ui_write(ui_cursor(this.y, this.x - 1) + color_input_scrollarrows + ((this.scroll > 0) ? "<" : " "));
	ui_write((this.insert ? color_input_ins : color_input) + this.value.substr(this.scroll, this.width) + ui_space(this.width - (this.length - this.scroll)));
	if (this.length - this.scroll > this.width)
		ui_write(ui_cursor(this.y, this.x + this.width - 1) + color_input_scrollarrows + ">");
	ui_write(ui_cursor(this.y, this.x + this.pos - this.scroll));
}

Input.prototype.onkey = function(key) {
	switch (key) {
		/* Escape sequences. */
		case "\x1B[D": /* Left. */
			this.left();
			return true;
		case "\x1B[C": /* Right. */
			this.right();
			return true;
		case "\x1B[A": /* Up. */
			this.history.prev();
			return true;
		case "\x1B[B": /* Down. */
			this.history.next();
			return true;
		case "\x1B[H": /* Home. */
			this.home();
			return true;
		case "\x1B[F": /* End. */
			this.end();
			return true;
		case "\x1B[2~": /* Insert. */
			this.insert = !this.insert;
			return true;
		case "\x1B[3~": /* Delete. */
			this.del();
			return true;
		/* Normal keys. */
		case "\x08":
		case "\x7F":
			/* Backspace. */
			this.backspace();
			return true;
		case "\x0A":
		case "\x0D":
			this.enter();
			return true;
		case "\x09": /* Tab. */
			this.completion.next();
			return true;
		default:
			if (key.charCodeAt(0) < 0x20) /* Skip non-printable stuff. */
				return false;
			this.write(key);
			return true;
	}
	return false;
}

/* Input history */
function InputHistory(input) {
	this.current = [ "" ]; /* Needs an item to represent current input */
	this.pos = 0; /* 0 = current input. */
	this.input = input;
}

InputHistory.prototype = new Array;

InputHistory.prototype.step = function() {
	this.input.completion.end();
	this.unshift(this.input.value);
	if (this.length > inputhistory_max)
		this.pop();
	this.current = this.slice();
	this.current.unshift("");
	this.pos = 0;
};

InputHistory.prototype.prev = function() {
	if (this.pos == this.current.length - 1)
		return;
	this.input.completion.end();
	this.input.scroll = 0;
	this.current[this.pos] = this.input.value;
	++this.pos;
	this.input.value = this.current[this.pos];
	this.input.pos = this.input.length;
	this.input.draw();
};

InputHistory.prototype.next = function() {
	if (this.pos == 0)
		return;
	this.input.completion.end();
	this.input.scroll = 0;
	this.current[this.pos] = this.input.value;
	--this.pos;
	this.input.value = this.current[this.pos];
	this.input.pos = this.input.length;
	this.input.draw();
};

/* Tab-completion. */
function InputCompletion(input, complete_empty, names_mode, append_space, candidates) {
	this.input = input;
	this.complete_empty = complete_empty;
	this.names_mode = names_mode;
	this.append_space = append_space;
	this.candidates = candidates || function() { return []; };
	this.started = false;
	this.pat = "";
	this.pos = 0;
	this.left = "";
	this.right = "";
	this.index = 0;
}

InputCompletion.prototype.next = function() {
	if (!this.started) {
		for (this.pos = this.input.pos; this.pos > 0; --this.pos)
			if (/[^a-z0-9_\-\[\]\\^{}|`]/i.test(this.input.charAt(this.pos - 1)))
				break;
		this.pat = this.input.substring(this.pos, this.input.pos).toLowerCase();
		this.left = this.input.substring(0, this.pos);
		this.right = this.input.substring(this.input.pos, this.input.length);
		this.index = -1;
		this.started = true;
	}
	if (!this.complete_empty && this.pat.length == 0)
		return;
	var candidates = this.candidates();
	for (var i = this.index + 1, n = 0; n < candidates.length; ++i, ++n) {
		var idx = i % candidates.length;
		if (candidates[idx].toLowerCase().startsWith(this.pat)) {
			var apd = ((this.pos == 0 && this.names_mode) ? ": " : (this.append_space ? " " : ""));
			this.input.value = this.left + candidates[idx] + apd + this.right;
			this.input.pos = this.left.length + candidates[idx].length + apd.length;
			this.index = idx;
			break;
		}
	}
	this.input.draw();
};

InputCompletion.prototype.end = function() {
	this.started = false;
};

/* Group of multiple tab-completion ways. */
function InputCompletions(input) {
	this.input = input;
}

InputCompletions.prototype = new Array;

InputCompletions.prototype.next = function() {
	for (var i = 0; i < this.length; ++i) {
		if (this[i].condition() || this[i].started) {
			this[i].next();
			break;
		}
	}
};

InputCompletions.prototype.end = function() {
	for (var i = 0; i < this.length; ++i)
		this[i].end();
};

InputCompletions.prototype.add = function(condition, complete_empty, names_mode, append_space, candidates) {
	var c = new InputCompletion(this.input, complete_empty, names_mode, append_space, candidates)
	c.condition = condition;
	this.push(c);
};
